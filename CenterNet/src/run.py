from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import _init_paths

import os
import cv2
from tqdm import tqdm
import pickle
import time
from os import path, listdir
from os.path import isfile, join
import numpy as np
from opts import opts
from detectors.detector_factory import detector_factory
from utils.debugger import Debugger
from PIL import Image
import json

def demo(opt):
    os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpus_str
    Detector = detector_factory[opt.task]
    detector = Detector(opt)

    files = [join(opt.demo, f) for f in listdir(opt.demo) if isfile(join(opt.demo, f))]

    num_images = len(files)
    annotations = []
    for i in tqdm(range(num_images)):
        ret = detector.run(files[i])['results']
        img_np = np.array(Image.open(files[i]))

        for category_id in ret.keys():
            for bbox in ret[category_id]:
                if bbox[-1] < 0.05:
                  continue
                annotation = {
                    "image_id": int(path.basename(files[i])[:-4]),
                    "category_id": category_id,
                    "bbox": list(map(float, bbox[:-1])),
                    "score": float(bbox[-1])
                }
                annotation["bbox"][2] -= annotation["bbox"][0]
                annotation["bbox"][3] -= annotation["bbox"][1]
                annotations.append(annotation)
    with open(opt.output_path, "w") as f:
        json.dump(annotations, f)

if __name__ == '__main__':
    start = time.time()
    opt = opts().init()
    demo(opt)
    end = time.time()
    print('total_time inference = ', end - start)
