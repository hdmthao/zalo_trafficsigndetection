from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import BatchNormalization
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import load_model
from skimage import transform
from skimage import exposure
from skimage import io
import sys
import os
import numpy as np
import cv2
from tqdm import tqdm
from PIL import Image
from os import listdir
from os.path import join, isfile
import json


class TrafficSignNet:
    @staticmethod
    def build(height, width, depth, classes):
        # initialize the model along with the input shape to be
        # "channels last" and the channels dimension itself
        model = Sequential()
        inputShape = (height, width, depth)
        chanDim = -1
        # CONV => RELU => BN => POOL
        model.add(Conv2D(8, (5, 5), padding="same",
                         input_shape=inputShape))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        # first set of (CONV => RELU => CONV => RELU) * 2 => POOL
        model.add(Conv2D(16, (3, 3), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(Conv2D(16, (3, 3), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        # second set of (CONV => RELU => CONV => RELU) * 2 => POOL
        model.add(Conv2D(32, (3, 3), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(Conv2D(32, (3, 3), padding="same"))
        model.add(Activation("relu"))
        model.add(BatchNormalization(axis=chanDim))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        # first set of FC => RELU layers
        model.add(Flatten())
        model.add(Dense(128))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(Dropout(0.5))
        # second set of FC => RELU layers
        model.add(Flatten())
        model.add(Dense(128))
        model.add(Activation("relu"))
        model.add(BatchNormalization())
        model.add(Dropout(0.5))
        # softmax classifier
        model.add(Dense(classes))
        model.add(Activation("softmax"))
        # model = EfficientNetB0(weights='imagenet', classes=classes)

        # return the constructed network architecture
        return model


annotation_path = sys.argv[1]
data_path = sys.argv[2]
model = load_model('../models/trafficsignnetdropout75.model')
imagePaths = [join(data_path, f)
                   for f in listdir(data_path) if isfile(join(data_path, f))]
with open(annotation_path) as f:
	data = json.load(f)
print("[INFO] detector detected {} bbox".format(len(data)))

new_data = data.copy()
counter = 0
for i in range(len(imagePaths)):
    imagePath = imagePaths[i]
    image_id = int(os.path.basename(imagePaths[i])[:-4])
    if i > 0 and i % 100 == 0:
        print("[INFO] predict {} total images".format(i))
	# load the image
    ori_image_np = np.array(Image.open(imagePath))
    for k, anno in enumerate(data):
        if anno["image_id"] != image_id:
            continue
        if sys.argv[3] == 'final' and anno["score"] > 0.3:
            continue
        bbox = anno["bbox"]

        x, y, w, h = list(map(int, bbox))
        x = max(x, 0)
        y = max(y, 0)
        w = max(w, 1)
        h = max(h, 1)
        crop_image_np = ori_image_np[y:y+h, x:x+w]
        cr_img_np = ori_image_np[y:y+h, x:x+w]
        if crop_image_np.shape[0] == 0 or crop_image_np.shape[1] == 0:
            continue
        crop_image_np = transform.resize(crop_image_np, (32, 32))
        try:
            crop_image_np = exposure.equalize_adapthist(crop_image_np, clip_limit=0.1)
        except Exception:
            continue
        crop_image_np = crop_image_np.astype("float32") / 255.0
        crop_image_np = np.expand_dims(crop_image_np, axis=0)
        preds = model.predict(crop_image_np)
        j = preds.argmax(axis=1)[0]
        new_data[k]["category_id"] = int(j)
with open(annotation_path[:-5] + "_classification.json", "w") as f:
    json.dump(new_data, f)
