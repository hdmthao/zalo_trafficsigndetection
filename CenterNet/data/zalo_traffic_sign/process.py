import json

with open('val.json') as f:
    data = json.load(f)

for anno in data["annotations"]:
    anno["category_id"] = 1

with open('val.json', 'w') as f:
    json.dump(data, f)

