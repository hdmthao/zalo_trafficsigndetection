import os
import sys
from os.path import exists, join, basename, splitext

CENTERNET_ROOT = '/zaloai/zalo_trafficsigndetection/CenterNet'
sys.path.insert(0, join(CENTERNET_ROOT, 'src/lib'))
sys.path.append(join(CENTERNET_ROOT, 'src'))
sys.path.append(join(CENTERNET_ROOT, 'src/lib/models/networks/DCNv2'))
